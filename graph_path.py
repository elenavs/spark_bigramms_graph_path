# Найдем длину кратчайшего пути между вершинами 12 и 34 ориентированного графа. Релазуем алгоритм "Поиск в ширину". Если кратчайших путей несколько, выводим первый.

from pyspark import SparkConf, SparkContext
sc = SparkContext(conf=SparkConf().setAppName("sparktask2").setMaster("yarn"))

def parse_edge(s):
    user, follower = s.split("\t")
    return (int(user), int(follower))

def step(item):
    prev_v, prev_d, next_v = item[0], item[1][0], item[1][1]
    return (next_v, prev_d + 1)

def complete(item):
    v, old_d, new_d = item[0], item[1][0], item[1][1]
    return (v, old_d if old_d is not None else new_d)

def path_step(item):
    last_vert, path, new_vert = item[0], item[1][0], item[1][1]
    last_vert = new_vert
    path += tuple([new_vert])
    return (last_vert, path)

n = 400
edges = sc.textFile("/data/twitter/twitter_sample.txt").map(parse_edge).cache()
forward_edges = edges.map(lambda e: (e[1], e[0])).partitionBy(n).persist()

x_start = 12
x_end = 34
d = 0

paths = sc.parallelize([ (x_start, tuple([x_start])) ]).partitionBy(n)

while True:
    d += 1
    paths = paths.join(forward_edges, n).map(path_step)
    if paths.filter(lambda x: x[0] == x_end).count() > 0:
        break

paths = paths.filter(lambda x: x[0] == x_end).collect()
min_path = paths[0][1]
min_path_len = len(min_path)
for path in paths:
    path = path[1]
    if len(path) < min_path_len:
        min_path = path
        min_path_len = len(min_path)

print(','.join(map(str, min_path)))
