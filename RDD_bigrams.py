# Найдем все биграммы, где первое слово «narodnaya» и посчитаем количество вхождений в статьях Википедии.

from pyspark import SparkContext, SparkConf
import re

config = SparkConf().setAppName("sparktask1").setMaster("yarn")
sc = SparkContext(conf=config)

rdd = sc.textFile("/data/wiki/en_articles_part")

rdd = rdd.map(lambda x: x.strip().lower()) 
rdd = rdd.map(lambda x: re.sub("narodnaya\W+", "narodnaya_", x)) 
rdd = rdd.flatMap(lambda x: x.split(" "))
rdd = rdd.map(lambda x: re.sub("^\W+|\W+$", "", x)) 
rdd = rdd.filter(lambda x: "narodnaya" in x) 
rdd = rdd.map(lambda x: (x, 1)) 
rdd = rdd.reduceByKey(lambda a, b: a + b).sortBy(lambda a: a[0], ascending=True)

words_count = rdd.collect()

for val in words_count:
    print('%s %s' % (val[0], val[1]))
